//
//  AdressCell.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Adress.h"
@interface AdressCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cep;
@property (strong, nonatomic) IBOutlet UILabel *lblStreet;
@property (strong, nonatomic) IBOutlet UILabel *lblDistrict;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblState;

-(void) setAdress:(Adress*) adress;

@end
