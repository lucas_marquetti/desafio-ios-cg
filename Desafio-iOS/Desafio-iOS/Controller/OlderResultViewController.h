//
//  OlderResultViewController.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OlderResultViewController : UIViewController

@property (nonatomic, strong)  NSMutableArray * arrayOldersSearchs;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
