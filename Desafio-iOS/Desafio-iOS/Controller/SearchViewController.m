//
//  ViewController.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 04/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "SearchViewController.h"
#import "ApiClient.h"
#import "Adress.h"
#import "OlderResultViewController.h"
#import "MaskedTextField.h"
#import "DAOAdress.h"
#import "DAOFolders.h"

@interface SearchViewController ()<ApiClientDelegate,UIAlertViewDelegate,UITextFieldDelegate>{
    Adress* _adress;
    NSMutableArray * _arrayOlders;
}
@property (strong, nonatomic) MaskedTextField* mask;

@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _arrayOlders = [[NSMutableArray alloc]init];
    
    UITapGestureRecognizer * tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiddenKey)];
    [self.view addGestureRecognizer:tapgesture];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions
- (IBAction)newSearch:(id)sender {
    
    NSData *asciiEncoded = [_txtAdress.text dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *cep = [[NSString alloc] initWithData:asciiEncoded encoding:NSASCIIStringEncoding];
    cep = [cep stringByReplacingOccurrencesOfString:@"." withString:@""];
    cep = [cep stringByReplacingOccurrencesOfString:@"-" withString:@""];
    cep = [cep stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([cep length]>3) {
        //Verifica se o cep ja foi pesquisado. Fazer a persistencia dos dados
        if ([self consultArray:cep]) {
            [ApiClient searchAdress:cep delegate:self];
        }else{
            [self insertValues];
        }
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Erro" message:@"A pesquise deve ter mais de 3 digítos" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
}

- (IBAction)olderSearch:(id)sender {
    
    [self performSegueWithIdentifier:@"oldSearchCep" sender:self];
}


#pragma mark - Verify Array
-(BOOL) addObjectPermission{
    BOOL perm = YES;
    
    for (Adress* adressSearch in _arrayOlders) {
        if ([adressSearch.street isEqualToString:_adress.street]) {
            perm = NO;
        }
    }
    return perm;
}

-(BOOL) consultArray:(NSString*) street{
    BOOL perm = YES;
    
    DAOAdress * op = [DAOAdress new];
    Adress *  arrayDB = [op getAdressWithStreet:street];
    NSMutableArray * array = [op getArrayAdressWithCep];
    for ( Adress *adresss in array) {
        if ([adresss.cep isEqualToString:street]) {
            perm = NO;
            _adress = adresss;
            break;
        }
    }
//    if (arrayDB) {
//        perm = NO;
//        _adress = arrayDB;
//    }

    return perm;
}


#pragma mark - HiddenKeyBoard
-(void) hiddenKey{
    [self.view endEditing:YES];
}

#pragma mark - InsertValueLabels
-(void) insertValues{
    
    _lblCountry.text = _adress.country;
    _lblStreet.text = _adress.street;
    _lblDistrict.text = _adress.district;
    _lblState.text = _adress.state;
    _lblCity.text = _adress.city;
}
#pragma mark - ApiDelegate
-(void)ApiClientDelegateDidFinish:(Adress*)result{
    _adress = result;
    
    [self insertValues];
    
    if ([self addObjectPermission]) {
        DAOAdress * op = [DAOAdress new];
        [op insertAdress:_adress];
        [_arrayOlders  addObject:_adress];
    }
}

-(void) ApiClientDelegateFailWithError:(NSString *)error{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Erro" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}
@end
