//
//  DAOFolders.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "DAOFolders.h"

#define STORAGE_FOLDER @"DESAFIO_FOLDER"

@implementation DAOFolders

+(NSString*)storagePath{
    static NSString *storagePath;
    if (!storagePath) {
        storagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:STORAGE_FOLDER];
    }
    return storagePath;
}

+(void)createMainFolders{

    NSString* _folderPath = @"";
    NSString* folderName = STORAGE_FOLDER;
    NSArray * documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectoryPath = [documentPaths objectAtIndex:0];
    _folderPath = [documentsDirectoryPath stringByAppendingPathComponent:folderName];
    
    NSError* error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    if (! [fileManager fileExistsAtPath:_folderPath isDirectory:&isDir]) {
        BOOL success = [fileManager createDirectoryAtPath:_folderPath withIntermediateDirectories:YES attributes:nil error:&error];
        if (!success || error) {
            NSLog(@"Erro: %@", [error localizedDescription]);
        }
        NSAssert(success, @"Failed to create folder at path:%@", _folderPath);
    }
    [DAOFolders createDirectoryAtStoragePathWithName:@"DB"];
}


+(BOOL)createDirectoryAtStoragePathWithName:(NSString*)name{
	NSFileManager *filemgr	= [NSFileManager defaultManager];
	NSString *newDir		= [[DAOFolders storagePath] stringByAppendingPathComponent:name];
	
	if ([filemgr changeCurrentDirectoryPath: newDir] == NO)
    {
		// nao existia
        if ([filemgr createDirectoryAtPath:newDir withIntermediateDirectories:YES attributes:nil error: NULL] == NO)
        {
			// falhou a criacao
			return NO;
        } else {
            // criou ok
            return YES;
        }
    }
	// ja existia
	return YES;
}
+(void)copyMainDBToDBFolder{
    NSError* error;
    
    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"DB/Desafio.sqlite"];

    if(![[NSFileManager defaultManager] fileExistsAtPath:[[DAOFolders storagePath] stringByAppendingPathComponent:@"DB/Desafio.sqlite"]]){
        [[NSFileManager defaultManager]
         copyItemAtPath:sourcePath
         toPath:[[DAOFolders storagePath] stringByAppendingPathComponent:@"DB/Desafio.sqlite"]
         error:&error];
        NSLog(@"Main DB: Base instalada.");
    }
    else{
        NSLog(@"Main DB: Já existe...");

    }
}

@end
