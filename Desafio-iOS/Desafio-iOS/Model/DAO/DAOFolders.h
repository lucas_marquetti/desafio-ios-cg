//
//  DAOFolders.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DAOFolders : NSObject

+(NSString*)storagePath;
+(void)createMainFolders;
+(BOOL)createDirectoryAtStoragePathWithName:(NSString*)name;
+(void)copyMainDBToDBFolder;

@end
