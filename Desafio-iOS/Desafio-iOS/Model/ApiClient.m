//
//  ApiClient.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "ApiClient.h"
#import "Adress.h"
#import "Reachability.h"
#import <AFNetworking/AFNetworking.h>

#define urlApi @"http://maps.google.com/maps/api/geocode/json?address="

@implementation ApiClient

+ (void)searchAdress: (NSString*)adress delegate:(id<ApiClientDelegate>)delegate
{
    Reachability *reach = [Reachability reachabilityForInternetConnection];

    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus != NotReachable){
        
        NSString* urlPost = [NSString stringWithFormat:@"%@%@",urlApi,adress];
        
        NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlPost parameters:nil error:nil ];
        
        AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        op.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id JSON) {
            
            NSLog(@"Result : %@",JSON);
            NSDictionary * json = JSON[@"results"][0];
            
            //Adress * adress = [[Adress alloc]initWithDictionary:json error:nil];
            Adress * adresss= [[Adress alloc]initWithDictionary:json];
            adresss.cep = adress;
            [delegate ApiClientDelegateDidFinish:adresss];
            
        } failure: ^(AFHTTPRequestOperation *operation, NSError * error) {
            
            if (error.code == -1011) {
                [delegate ApiClientDelegateFailWithError:@"Endereço não encontrado"];
            }else if (error.code == 503){
                [delegate ApiClientDelegateFailWithError:@"Serviço indisponível"];
            }
            
        }];
        [[NSOperationQueue mainQueue] addOperation:op];
    }else{
        [delegate ApiClientDelegateFailWithError:@"Não possui conexão com a internet."];
    }

}

@end
