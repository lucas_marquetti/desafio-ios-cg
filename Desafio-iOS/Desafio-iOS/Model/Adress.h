//
//  Adress.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
@interface Adress : MTLModel<MTLJSONSerializing>

#pragma mark - propertys

@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* state;
@property (nonatomic, strong) NSString* district;
@property (nonatomic, strong) NSString* country;
@property (nonatomic, strong) NSString* cep;
@property (nonatomic, strong) NSString* typeAdress;
@property (nonatomic, strong) NSString* street;

@property (nonatomic, strong) NSString* long_name;
@property (nonatomic, strong) NSString* short_name;


@property (nonatomic, strong) NSString* partial_match;
@property (nonatomic, strong) NSString* route;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* location_type;

@property (nonatomic, strong) NSString* formatted_adress;
@property (nonatomic, strong) NSDictionary* southwest;
@property (nonatomic, strong) NSDictionary* northeast;

@property (nonatomic, strong) NSArray* address_components;
@property (nonatomic, strong) NSArray* geometry;
@property (nonatomic, strong) NSArray* viewport;
@property (nonatomic, strong) NSArray* bounds;
@property (nonatomic, strong) NSArray* types;

@property (nonatomic) float lat;
@property (nonatomic) float lng;


#pragma mark - methods

+ (NSDictionary *)JSONKeyPathsByPropertyKey;
-(id) initWithDictionary : (NSDictionary*) dict;
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error;

@end
