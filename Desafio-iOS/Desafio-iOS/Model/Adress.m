//
//  Adress.m
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 05/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import "Adress.h"


@implementation Adress



#pragma mark - init the self
-(id) initWithDictionary : (NSDictionary*) dict{
    
    _address_components = [[NSArray alloc]initWithObjects: dict[@"address_components"], nil];
    _formatted_adress = dict[@"formatted_address"];
    _partial_match = dict[@"partial_match"];
    _geometry = [[NSArray alloc]initWithObjects: dict[@"geometry"], nil];
    _types = [[NSArray alloc]initWithObjects: dict[@"types"], nil];
    
    
    //_cep = dict[@"cep"];
    //_typeAdress = dict[@"tipoDeLogradouro"];
    _district = _address_components[0][1][@"long_name"];
    _state = _address_components[0][4][@"short_name"];
    _street = _address_components[0][0][@"long_name"];
    _country = _address_components[0][5][@"long_name"];
    _city = _address_components[0][2][@"long_name"];
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    
    return self;
}

@end

