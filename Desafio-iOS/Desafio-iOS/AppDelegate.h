//
//  AppDelegate.h
//  Desafio-iOS
//
//  Created by Lucas Marquetti on 04/09/14.
//  Copyright (c) 2014 Lucas Marquetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
