

### **Iten a fazer** ###
1.REGULAR EXPRESSIONS
Escreva uma aplicação em Java (console application) que recebe como argumento um diretório. A aplicação deverá varrer o diretório (incluindo subdiretórios) em busca de arquivos textuais que tenham em seu conteúdo números telefônicos. Uma vez encontrados os números telefônicos, estes deverão ser impressos no terminal com formatação corrigida (Ex.: 067 81121717 ou 011 953747949). São considerados formatos válidos de telefones:
(67) X (67)X 067 X 067X 0 67 X 0 67X 67 X 67X
‘X’ pode ser:
para números de 8 dígitos:
81121717 8112.1717 8112-1717 8112 1717
para números de 9 dígitos: 953-747-949
953.747.949 953 747 949 95374-7949 95374.7949 95374 7949 9-5374-7949 9.5374.7949 9 5374 7949


2.LAYOUTS
Reproduza a seguinte tela num aplicativo iOS:
* Atente para o fato de que cada cor representa um elemento distinto. Observe ainda os alinhamentos, tamanhos (proporções) e posições relativas dos itens. Utilize os arquivos em anexo.

3.NOSQL, APIs, ETC
Encontre uma biblioteca de Objective-C que implementa o conceito de key/value database (Ex.: Level DB) e a utilize numa aplicação de exemplo que faz uma chamada a http://maps.google.com/maps/api/geocode/json?address={um-endereco-qualquer} e armazena o resultado para uso posterior. Apenas para efeito demonstrativo, faça a chamada utilizando POST e enviando no corpo da mensagem um JSON de um objeto qualquer.

4. JAVA GENERICS
Escreva uma classe genérica (SuperList<T>) derivada de ArrayList<T> que implemente os seguintes métodos:
T max1;
T min1;
SuperList<T> where2 (devolve uma lista com os elementos que atendem a condição determinada); T first2 (devolve a primeira ocorrência para uma determinada condição);
boolean any2 (devolve true caso haja alguma ocorrência para a condição determinada);
boolean all2 (devolve true caso todos os elementos da lista atendam a condição determinada);
1 recebem como parâmetro uma “função” genérica que devolve um Comparable para que se encontre o maior ou menor elemento de acordo com o requisito desejado. Exemplo de chamada:
Person tallestPerson = people.max(
new SuperItemProcessor<Person, Comparable> {
@Override public Comparable doIt(Person p) { return p.height;
} }
);
2 recebem como parâmetro uma “função” que devolve um Boolean avaliando se um determinado elemento da lista atende a um requisito desejado. Exemplo de chamada:
SuperList<User> gmailUsers = users.where(
new SuperItemProcessor <User, Boolean>() {
@Override Boolean doIt(User user) {
return user.email.endsWith(“@gmail.com”);
} }
);


5. CONCEITOS
a. Quando e por que utilizar escopo de transação com o banco de dados?
b. Qual a diferença entre lazy e eager loading? Quando a utilização de lazy loading deve ser evitada?
c. Como você implementaria um serviço capaz de detectar quando o usuário provavelmente está dirigindo (utilize somente recursos disponíveis no smartphone e evite consumo excessivo de bateria)?
d. Como você implementaria um serviço capaz de determinar o momento em que um usuário está provavelmente prestes a sair de casa (utilize somente recursos disponíveis no smartphone)?